package com.example.controller

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.controller.TaskController.Companion.TASK_PATH
import com.example.data.TestData.Companion.TEST_MSG_1
import com.example.model.TaskRequest
import com.example.model.TaskResponse
import com.example.service.TaskService
import com.example.support.APIGatewayV2ProxyRequestEventBuilder
import com.example.support.LambdaSupportUtil
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TaskControllerTest {
    @Mock
    lateinit var taskService: TaskService

    @InjectMocks
    lateinit var taskController: TaskController

    private fun <T> anyObject(): T = Mockito.anyObject<T>()
    fun <T> any(): T = Mockito.any<T>()

    @Test
    fun testGet() {
        val request = TaskRequest(id = 1, message = TEST_MSG_1)
        val response = TaskResponse(4)

        val apiRequest = APIGatewayV2ProxyRequestEventBuilder.builder()
            .path(TASK_PATH)
            .httpMethod(HttpMethod.GET)
            .body(request)
            .build()


//        `when`(taskService.performTask(anyObject()))
//            .thenReturn(response)

        val apiResponse: APIGatewayV2ProxyResponseEvent? =
            taskController.handleRequest(apiRequest, mock(Context::class.java))

        val actual = LambdaSupportUtil.readFromString(apiResponse?.body, TaskResponse::class.java)!!

//        assertEquals(4, actual.count)
    }
}
