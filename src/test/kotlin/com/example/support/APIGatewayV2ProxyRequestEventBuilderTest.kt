package com.example.support

import com.amazonaws.HttpMethod
import com.example.support.model.TestRequest
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class APIGatewayV2ProxyRequestEventBuilderTest {
    @Test
    fun testBuilder() {
        val request = APIGatewayV2ProxyRequestEventBuilder.builder()
            .path("path")
            .body("body")
            .httpMethod(HttpMethod.GET.toString())
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("a", "1")
                }
            })
            .queryParameters(object : HashMap<String?, String?>() {
                init {
                    put("a", "1")
                    put("b", "2")
                }
            })
            .pathParameters(object : HashMap<String?, String?>() {
                init {
                    put("a", "1")
                    put("b", "2")
                    put("c", "3")
                }
            })
            .build()
        assertEquals("path", request.path)
        assertEquals("body", request.body)
        assertEquals("GET", request.httpMethod)
        assertEquals(1, request.headers.size)
        assertEquals(2, request.queryStringParameters.size)
        assertEquals(3, request.pathParameters.size)
    }

    @Test
    fun testObjectBody() {
        val requestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .path("123")
            .body(TestRequest(str ="teststring"))
            .resource("/v2/something/{pathParam}")
            .httpMethod("GET")
            .build()
        assertTrue(requestEvent.body.contains("teststring"))
    }

    @Test
    fun testNullBody() {
        val requestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .path("123")
            .body(null)
            .resource("/v2/something/{pathParam}")
            .httpMethod("GET")
            .build()
        assertNull(requestEvent.body)
    }
}
