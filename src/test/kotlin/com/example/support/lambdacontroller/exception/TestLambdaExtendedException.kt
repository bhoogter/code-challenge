package com.example.support.lambdacontroller.exception

import com.example.support.exception.LambdaControllerException


class TestLambdaExtendedException : LambdaControllerException {
    companion object {
        const val MESSAGE = "test lambda exception"
    }

    constructor() : super(msg = MESSAGE) {}
    constructor(status: Int) : super(status, MESSAGE) {}
}
