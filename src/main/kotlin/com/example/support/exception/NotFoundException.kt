package com.example.support.exception

class NotFoundException : RuntimeException {
    constructor(message: String?) : super(message) {}
    constructor(message: String?, t: Throwable?) : super(message, t) {}
}