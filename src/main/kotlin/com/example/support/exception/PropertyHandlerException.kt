package com.example.support.exception

class PropertyHandlerException : RuntimeException {
    constructor(errorMessage: String?) : super(errorMessage) {}
    constructor(errorMessage: String?, t: Throwable?) : super(errorMessage, t) {}
}