package com.example.support.exception

class StringToObjectException : RuntimeException {
    constructor(message: String?) : super(message)
    constructor(message: String?, t: Throwable?) : super(message, t)
}