package com.example.service

import com.example.model.TaskRequest
import com.example.model.TaskResponse
import com.example.repository.TaskRepository
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TaskServiceTest {

    @Mock
    lateinit var taskRepository: TaskRepository

    @InjectMocks
    lateinit var taskService: TaskService

    @Test
    fun testPerformTask_doProcess() {
        val msg = "test message"
        val request = TaskRequest(44, msg)
        val mockResponse = TaskResponse(38)

//        `when`(taskRepository.isProcessed(any()))
//            .thenReturn(false)
//        `when`(taskRepository.process(any(), any()))
//            .thenReturn(55)

//        val captureInt = ArgumentCaptor.forClass(Int::class.java)
//        val captureStr = ArgumentCaptor.forClass(String::class.java)

//        val response = taskService.performTask(request)

//        verify(taskRepository, times(1))
//            .isProcessed(eq(44))
////        verify(taskRepository, times(1))
//            .process(captureInt.capture(), captureStr.capture())

//        assertEquals(44, captureInt.value)
//        assertEquals(msg, captureStr.value)
//        assertEquals(55, response)
    }
}
