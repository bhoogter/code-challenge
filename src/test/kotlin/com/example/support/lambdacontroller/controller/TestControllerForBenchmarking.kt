package com.example.support.lambdacontroller.controller

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.support.APIGatewayV2ProxyResponseEventBuilder
import com.example.support.LambdaController
import com.example.support.lambdacontroller.LambdaControllerTest
import com.example.support.lambdacontroller.TestLambdaException
import com.example.support.lambdacontroller.exception.TestLambdaExtendedException
import com.example.support.model.ErrorResponse
import com.example.support.model.TestRequest
import java.time.ZonedDateTime

class TestControllerForBenchmarking : LambdaController() {
    @LambdaGet(CHECK)
    fun doNoopGet(): String {
        return "Ok - GET"
    }

    @LambdaPost(CHECK)
    fun doNoopPost(): String {
        return "Ok - POST"
    }

    @LambdaDelete(CHECK)
    fun doNoopDelete(): String {
        return "Ok - DELETE"
    }

    @LambdaExceptionHandler(TestLambdaException::class)
    fun handleTestException(t: TestLambdaException?): APIGatewayV2ProxyResponseEvent {
        handledError = true
        return APIGatewayV2ProxyResponseEventBuilder.builder()
            .body(ErrorResponse(ZonedDateTime.now(), LambdaControllerTest.ERROR_TEXT))
            .build()
    }

    @LambdaExceptionHandler(TestLambdaExtendedException::class)
    fun handleTestException2(t: Throwable?): String {
        return TestLambdaExtendedException.MESSAGE
    }

    companion object {
        const val CHECK = "/check"
        var lastRequest: TestRequest? = null
        var handledError = false
    }
}