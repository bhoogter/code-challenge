package com.example.controller

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.exception.TaskDataException
import com.example.exception.TaskException
import com.example.model.TaskRequest
import com.example.model.TaskResponse
import com.example.service.TaskService
import com.example.support.LambdaController
import com.example.support.LambdaSupportUtil
import org.apache.http.HttpStatus.SC_BAD_REQUEST
import org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR

open class TaskController : LambdaController {
    companion object {
        const val TASK_PATH = "/v1/task"
    }

    private var taskService: TaskService

    constructor() : this(TaskService())
    constructor(taskService: TaskService) : super() {
        this.taskService = taskService
    }

    @LambdaGet(TASK_PATH)
    fun getTaskResponse(request: TaskRequest): TaskResponse {
        return taskService.performTask(request)
    }

    @LambdaExceptionHandler(TaskException::class)
    fun handleTaskException(ex: TaskException): APIGatewayV2ProxyResponseEvent {
        return LambdaSupportUtil.errorResponseEvent("Task Exception: ${ex.message}", SC_BAD_REQUEST)
    }

    @LambdaExceptionHandler(TaskDataException::class)
    fun handleTaskDataException(ex: TaskException): APIGatewayV2ProxyResponseEvent {
        return LambdaSupportUtil.errorResponseEvent("Task DataException: ${ex.message}", SC_INTERNAL_SERVER_ERROR)
    }
}
