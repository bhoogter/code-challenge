package com.example.support.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.ZonedDateTime

class DateHolder {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    var date: ZonedDateTime? = null
}