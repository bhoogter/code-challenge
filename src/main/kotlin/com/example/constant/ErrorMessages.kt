package com.example.constant

class ErrorMessages {
    companion object {
        const val ERROR_CONVERTING_STRING_TO_OBJECT = "Error converting string to object, %s: %s"
        const val ERROR_CONVERTING_OBJECT_TO_STRING = "Error converting object to string, %s: %s"
        const val ERROR_CONVERTING_OBJECT_TO_BYTES = "Error converting object to string, %s: %s"
    }
}
