package com.example.support.model

import java.util.*

data class TestRequest(
    var i: Int? = null,
    var d: Double? = null,
    var str: String? = null,
    var f: UUID? = null,
    var b: Boolean? = null
)