package com.example.support

import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PropertyHandlerTest {
    @Test
    fun testGetEnv() {
        assertNull(PropertyHandler.getSystemEnv("LIBPATH"))
    }

    @Test
    fun testLoadYaml() {
        val result: Map<String, Any>?
        result = PropertyHandler.loadYaml("application-test.yml")
        assertNotNull(result)
        assertTrue(result!!.size > 0)
    }

    @Test
    fun testLoadYamlFailure() {
        assertNull(PropertyHandler.loadYaml("does-no-exist.yml"))
    }

    @Test
    fun testGetNonExistentProperty() {
        assertNull(PropertyHandler.getPropertyString("does.not.-exist-"))
    }

    @Test
    fun testGetArray() {
        val stringResult = PropertyHandler.getPropertyString("test.arr")
        val arrayResult = PropertyHandler.getProperty("test.arr") as List<*>?
        assertEquals("[1, 2, 3]", stringResult)
        assertEquals(3, arrayResult?.size ?: 0)
    }

    @Test
    fun testGetPropertyFromTestYaml() {
        assertEquals("is test", PropertyHandler.getPropertyString("test.string"))
        assertEquals(33, PropertyHandler.getPropertyInt("test.int"))
        assertEquals(true, PropertyHandler.getPropertyBool("test.bool"))
    }

    @Test
    fun testGetPropertyFromTestYaml_defaults() {
        assertEquals("is na test", PropertyHandler.getPropertyString("test.na-string", "is na test"))
        assertEquals(34, PropertyHandler.getPropertyInt("test.na-int", 34))
        assertEquals(false, PropertyHandler.getPropertyBool("test.na-bool", false))
    }

    @Test
    fun testSetPropertyString() {
        PropertyHandler.setPropertyString("test-add", "value")
        assertEquals("value", PropertyHandler.getPropertyString("test-add"))
    }

    @Test
    fun testEnvSetting() {
        PropertyHandler.environment = "alt"
        assertEquals(4, PropertyHandler.getPropertyInt("test.alt"))
    }
}
