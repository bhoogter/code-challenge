package com.example.support

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.example.support.LambdaSupportUtil.Companion.writeToString

class APIGatewayV2ProxyRequestEventBuilder private constructor() {
    private val instance: APIGatewayV2ProxyRequestEvent
    fun build(): APIGatewayV2ProxyRequestEvent {
        return instance
    }

    fun httpMethod(s: HttpMethod): APIGatewayV2ProxyRequestEventBuilder {
        instance.httpMethod = s.toString()
        return this
    }

    fun httpMethod(s: String?): APIGatewayV2ProxyRequestEventBuilder {
        instance.httpMethod = s
        return this
    }

    fun path(s: String?): APIGatewayV2ProxyRequestEventBuilder {
        instance.path = s
        return this
    }

    fun resource(s: String?): APIGatewayV2ProxyRequestEventBuilder {
        instance.resource = s
        return this
    }

    fun body(s: String?): APIGatewayV2ProxyRequestEventBuilder {
        instance.body = s
        return this
    }

    fun body(o: Any?): APIGatewayV2ProxyRequestEventBuilder {
        instance.body = writeToString(o)
        return this
    }

    fun headers(h: Map<String?, String?>?): APIGatewayV2ProxyRequestEventBuilder {
        instance.headers = h
        return this
    }

    fun pathParameters(v: Map<String?, String?>?): APIGatewayV2ProxyRequestEventBuilder {
        instance.pathParameters = v
        return this
    }

    fun queryParameters(q: Map<String?, String?>?): APIGatewayV2ProxyRequestEventBuilder {
        instance.queryStringParameters = q
        return this
    }

    companion object {
        fun builder(): APIGatewayV2ProxyRequestEventBuilder {
            return APIGatewayV2ProxyRequestEventBuilder()
        }

        fun toString(req: APIGatewayV2ProxyRequestEvent?): String? {
            return writeToString(req)
        }
    }

    init {
        // No Clone
        instance = APIGatewayV2ProxyRequestEvent()
    }
}