package com.example.support.lambdacontroller.enums

enum class TestControllerEnum {
    VALUE1, VALUE2, VALUE3
}