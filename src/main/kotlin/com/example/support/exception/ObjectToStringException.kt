package com.example.support.exception

class ObjectToStringException : RuntimeException {
    constructor(message: String?) : super(message) {}
    constructor(message: String?, t: Throwable?) : super(message, t) {}
}