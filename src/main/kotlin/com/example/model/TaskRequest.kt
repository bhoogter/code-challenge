package com.example.model

data class TaskRequest(
    val id: Int = 0,
    val message: String = ""
)