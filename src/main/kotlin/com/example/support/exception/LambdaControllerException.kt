package com.example.support.exception

import java.lang.RuntimeException

open class LambdaControllerException : RuntimeException {
    val status: Int
    constructor(
        status: Int = 500,
        msg: String = "Error",
        t: Throwable? = null
    ) : super(msg, t) {
        this.status = status
    }
}
