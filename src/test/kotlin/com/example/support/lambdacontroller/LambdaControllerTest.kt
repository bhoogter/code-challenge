package com.example.support.lambdacontroller

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.example.support.APIGatewayV2ProxyRequestEventBuilder
import com.example.support.LambdaSupportUtil
import com.example.support.exception.LambdaControllerException
import com.example.support.lambdacontroller.controller.TestController
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_DELETE_ERROR_2
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_GET_QS
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_GET_RESPONSE
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_GET_RESPONSE_STRINGS
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_GET_RESPONSE_VIN1
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_PATCH_ERROR_3
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_POST_VOID
import com.example.support.lambdacontroller.controller.TestController.Companion.V_1_PUT_ERROR
import com.example.support.lambdacontroller.exception.TestLambdaExtendedException
import com.example.support.model.ErrorResponse
import com.example.support.model.TestRequest
import com.example.support.model.TestResponse
import junit.framework.TestCase
import org.apache.http.HttpStatus
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class LambdaControllerTest : TestCase() {
    private lateinit var testController: TestController

    @Before
    fun setup() {
        testController = TestController()
    }

    @Test
    fun testGetResponse() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.GET.toString())
            .path(V_1_GET_RESPONSE)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("x-uuid", TEST_UUID)
                    put("X-Tenant-Id", TEST_UUID + "," + UUID.randomUUID().toString())
                }
            })
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        val response: TestResponse? = LambdaSupportUtil.readFromString(
            apiResponse?.body,
            TestResponse::class.java
        )
        assertNotNull(response)
        assertEquals(testResponse().jurisdiction, response?.jurisdiction)
    }

    @Test
    fun testGetResponseStrings() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.GET.toString())
            .path(V_1_GET_RESPONSE_STRINGS)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("x-uuid", TEST_UUID)
                    put("X-Tenant-Id", TEST_UUID + "," + UUID.randomUUID().toString())
                }
            })
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        val response: TestResponse? = LambdaSupportUtil.readFromString(
            apiResponse?.body,
            TestResponse::class.java
        )
        assertNotNull(response)
        assertEquals(testResponse().jurisdiction, response?.jurisdiction)
    }

    @Test
    fun testGetResponseVin() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.GET.toString())
            .path(V_1_GET_RESPONSE_VIN1)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("X-Tenant-Id", TEST_UUID + "," + UUID.randomUUID().toString())
                }
            })
            .pathParameters(object : HashMap<String?, String?>() {
                init {
                    put("vin", TEST_VIN)
                }
            })
            .build()
        val apiResponse = testController.handleRequest(
            apiRequest, Mockito.mock(
                Context::class.java
            )
        )
        val response = LambdaSupportUtil.readFromString(
            apiResponse?.body,
            TestResponse::class.java
        )
        assertNotNull(response)
        assertEquals(TEST_VIN, response?.jurisdiction)
    }

    @Test
    fun testPostResponse_postDoesntExist_expectNotFound() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.POST.toString())
            .path(V_1_GET_RESPONSE)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("x-uuid", TEST_UUID)
                    put("X-Tenant-Id", TEST_UUID + "," + UUID.randomUUID().toString())
                }
            })
            .build()
        val apiResponse = testController.handleRequest(
            apiRequest, Mockito.mock(
                Context::class.java
            )
        )
        assertEquals(HttpStatus.SC_NOT_FOUND, apiResponse?.statusCode)
    }

    @Test
    fun testErrorHandler() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.PUT.toString())
            .path(V_1_PUT_ERROR)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .build()
        TestController.lastRequest = null
        TestController.handledError = false
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        val responseBody = apiResponse?.body
        val response = LambdaSupportUtil.readFromString(responseBody, ErrorResponse::class.java)
        assertNotNull(TestController.lastRequest)
        assertNotNull(response?.timeStamp)
        assertEquals(ERROR_TEXT, response?.errorMessage)
        assertTrue(TestController.handledError)
    }

    @Test
    fun testErrorHandler2() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.DELETE.toString())
            .path(V_1_DELETE_ERROR_2)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertEquals(HttpStatus.SC_UNPROCESSABLE_ENTITY, apiResponse?.statusCode)
        assertEquals(
            TestLambdaExtendedException.Companion.MESSAGE,
            LambdaSupportUtil.readFromString(apiResponse?.body, ErrorResponse::class.java)?.errorMessage
        )
    }

    @Test
    fun testUnknownError() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.PATCH.toString())
            .path(V_1_PATCH_ERROR_3)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, apiResponse?.statusCode)
        assertTrue(apiResponse?.body?.contains("Error handling request") ?: false)
    }

    @Test
    fun testGetQueryString() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.GET.toString())
            .path(V_1_GET_QS)
            .queryParameters(object : HashMap<String?, String?>() {
                init {
                    put("four", "4")
                    put("serviceAreaId", UUID.randomUUID().toString())
                }
            })
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertNotNull(apiResponse)
    }

    @Test
    fun testPostVoid() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.POST.toString())
            .path(V_1_POST_VOID)
            .body(LambdaSupportUtil.writeToString(testRequest()))
            .build()
        val apiResponse = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertNull(apiResponse)
    }

    @Test
    fun testPostQStoEnum() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.POST.toString())
            .path(V_1_GET_QS)
            .queryParameters(object : HashMap<String?, String?>() {
                init {
                    put("enum", "VALUE2")
                }
            }) //                .body("")
            .build()
        val result = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, result?.statusCode)
        assertTrue(result?.body?.contains("wrong enum detected") ?: false)
    }

    @Test
    fun testPostQStoEnum_invalidValue_giveIAE() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.POST.toString())
            .path(V_1_GET_QS)
            .queryParameters(object : HashMap<String?, String?>() {
                init {
                    put("enum", "invalid-value")
                }
            }) //                .body("")
            .build()
        val result = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertEquals(HttpStatus.SC_BAD_REQUEST, result?.statusCode)
        assertTrue(result?.body?.contains("No enum constant") ?: false)
    }

    @Test
    fun testPostQStoEnum_noValue_giveNull() {
        val apiRequest: APIGatewayV2ProxyRequestEvent = APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(HttpMethod.POST.toString())
            .path(V_1_GET_QS)
            .build()
        val result = testController.handleRequest(apiRequest, Mockito.mock(Context::class.java))
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, result?.statusCode)
        assertTrue(result?.body?.contains("no enum passed") ?: false)
    }

    @Test
    fun testWarmupDisabling() {
        testController.setWarmupEnabled(false)
        assertFalse(testController.warmupEnabled)
        testController.setWarmupEnabled(true)
        assertTrue(testController.warmupEnabled)
    }

    companion object {
        const val TEST_UUID = "94dd927b-942e-4ff5-8c7b-b50899f5efe2"
        const val TEST_VIN = "12345678901234567"
        const val ERROR_TEXT = "error text"
        fun testRequest(): TestRequest {
            return TestRequest(1, 3.4, "x", UUID.fromString(TEST_UUID), true)
        }

        fun testResponse(): TestResponse {
            return TestResponse(Arrays.asList("1", "2", "3", "4"), emptyList(), "broad", 3.8, false)
        }

        fun testException(): LambdaControllerException {
            return testException(HttpStatus.SC_UNPROCESSABLE_ENTITY, ERROR_TEXT)
        }

        fun testException(status: Int, msg: String): LambdaControllerException {
            return LambdaControllerException(status, msg)
        }
    }
}