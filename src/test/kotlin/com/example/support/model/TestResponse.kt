package com.example.support.model

class TestResponse(
    var values: List<String>? = null,
    var valuesSub: List<TestResponseSub>? = null,
    var jurisdiction: String? = null,
    var viability: Double? = null,
    var valid: Boolean? = null
)