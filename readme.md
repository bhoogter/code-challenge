# Code-Challenge

What exists in this repo is an AWS ready REST API written in Kotlin.
  
The code can be run and tested locally, using the file:

> `/src/test/kotlin/com/example/integration/TaskControllerIT.kt`

Or, if preferred, can be uploaded as an AWS lambda, enabled
using an AWS gateway, and the `LambdaController` pattern
should route the event effectively through the `TaskController` file
located int he repo.

The integration test case, above, utilizes the `APIGatewayV2ProxyEvent`
interface provided by the AWS APIm and routes the event
in a Spring-like manner, but without the overhead of a
Spring app.

## Documentation

Typically, small comments are left in the code.  However, due
to the scope of the work, the test cases adequately ddocuemnt
the function.

Additionally, the Mockito Unit tests are disabled.

## Additional Imrovements

Ideally, the data store should be backed
by a real data source, such as dynamo or an RDS db.

Due to the lack of a full AWS stack to deploy,
it uses a simple in-memory store, which, of course,
would not persist once the lambda unloaded, or
across instances.