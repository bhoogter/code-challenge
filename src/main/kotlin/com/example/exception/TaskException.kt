package com.example.exception

class TaskException : RuntimeException {
    constructor(message: String?) : super(message)
    constructor(message: String?, t: Throwable?) : super(message, t)
}