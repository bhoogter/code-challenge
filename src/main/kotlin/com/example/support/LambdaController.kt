package com.example.support

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.constant.HeaderConstants.Companion.LAMBDA_VERSION_KEY
import com.example.support.exception.*
import org.apache.http.HttpStatus.*
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.lang.reflect.Parameter
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.HashMap
import kotlin.reflect.KClass

abstract class LambdaController : RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent?> {
    private val logger = LoggerFactory.getLogger(javaClass)

    var warmupEnabled = true
        private set

    fun setWarmupEnabled(value: Boolean): Boolean {
        warmupEnabled = value
        return warmupEnabled
    }

    /**
     * Handles routing the request (and hence, 404s).
     *
     *
     * This is the function called by AWS on an API request.
     *
     * @param input
     * @param context
     * @return APIGatewayV2ProxyResponseEvent that is actually returned to the client.
     */
    override fun handleRequest(
        input: APIGatewayV2ProxyRequestEvent,
        context: Context
    ): APIGatewayV2ProxyResponseEvent? {
        var response: APIGatewayV2ProxyResponseEvent?
        logger.info("Request Event: [{}] - {}", input.httpMethod, input.path)
        if (input.body != null) logger.info("Request Body: {}", input.body)
        logger.trace(APIGatewayV2ProxyRequestEventBuilder.toString(input))
        val pathParams: Map<String, String> = if (input.pathParameters == null) mutableMapOf() else input.pathParameters
        val m: Method? = lambdaHandler(input.httpMethod, input.path, input.resource, pathParams)
        response = if (m != null) {
            logger.info("LambdaController: Handler Found [{}]", m.name)
            wrapRequest(m, input, context)
        } else {
            logger.warn("LambdaController: NOT FOUND: [{}] {}", input.httpMethod, input.path)
            logger.info(
                "APIGatewayV2ProxyRequestEvent: " + APIGatewayV2ProxyRequestEventBuilder.toString(
                    input
                )
            )
            LambdaSupportUtil.errorResponseEvent("Not Found", SC_NOT_FOUND)
        }
        response = addLambdaHeaders(response)
        logger.info("Response Sent: {}", response)
        return response
    }

    /**
     * Wrap the actual request with our pre and post-processors
     *
     * @param m
     * @param input
     * @param context
     * @return APIGatewayV2ProxyResponseEvent representing the pre and post-processed result of the handler.
     */
    fun wrapRequest(
        m: Method,
        input: APIGatewayV2ProxyRequestEvent,
        context: Context
    ): APIGatewayV2ProxyResponseEvent? {
        var response: APIGatewayV2ProxyResponseEvent?
        try {
            logger.info("LambdaController: Pre-Processing Request")
            preProcessEvent(input, context)
            logger.info("LambdaController: Executing Request")
            response = executeRequestInternal(m, input, context)
            logger.info("LambdaController: Post-Processing Response")
            postProcessEvent(input, context, response)
        } catch (e: Throwable) {
            response = handleError(e, m, input, context)
        }
        return response
    }

    /**
     * Execute a given request input on the given handler method m.  Handles void return (returns null), as well as
     * unifying all others to an APIGatewayV2ProxyResponseEvent.
     *
     * @param m
     * @param input
     * @param context
     * @return APIGatewayV2ProxyResponseEvent representing the raw handler response.
     */
    private fun executeRequestInternal(
        m: Method,
        input: APIGatewayV2ProxyRequestEvent,
        context: Context
    ): APIGatewayV2ProxyResponseEvent? {
        val apiResponse: APIGatewayV2ProxyResponseEvent?
        logger.info("LambdaController: Executing handler [{}]", m.getName())
        val response = invokeWithParams(m, input, context, null)
        if (m.returnType.name == "void" || m.returnType === Void::class.java) {
            logger.info("Void return type.  Exiting by returning null.")
            return null
        }
        if (response is APIGatewayV2ProxyResponseEvent) {
            logger.info("LambdaController: Handler returned APIGatewayV2ProxyResponseEvent.")
            apiResponse = response
        } else if (response is String) {
            logger.info("LambdaController: Handler returned string.")
            apiResponse = LambdaSupportUtil.responseEvent(methodStatusCode(m), response)
        } else {
            val name = response?.javaClass?.simpleName ?: "unknown"
            logger.info("LambdaController: Handler returned $name.")
            apiResponse = LambdaSupportUtil.responseEvent(methodStatusCode(m), response)
        }
        logger.info("Raw Result Event: {}", apiResponse)
        return apiResponse
    }

    /**
     * Adds custom header(s) for Lambda Controller identification.
     *
     * @param response
     * @return
     */
    private fun addLambdaHeaders(response: APIGatewayV2ProxyResponseEvent?): APIGatewayV2ProxyResponseEvent? {
        if (response == null) return null
        if (response.headers == null) response.headers = HashMap()
        var version: String? = this.javaClass.getPackage().getImplementationVersion()
        if (version == null || version.isEmpty()) version = PropertyHandler.getPropertyString("lambdaVersion")
        if (version == null || version.isEmpty()) version = "UNKNOWN"
        val hash = PropertyHandler.getPropertyString("commitHash", "UNKNOWN")
        val value: String = this.javaClass.getSimpleName().toString() + " " + version + " [" + hash + "]"
        response.headers.put(LAMBDA_VERSION_KEY, value)
        return response
    }

    /**
     * Determines the success status code for a give handler method.
     *
     *
     * Default return value for all methods is 200 (SC_OK).
     *
     * @param m
     * @return int
     */
    protected fun methodStatusCode(m: Method): Int {
        for (a in m.getAnnotations()) {
            if (a is LambdaGet) return a.successCode
            if (a is LambdaPost) return a.successCode
            if (a is LambdaDelete) return a.successCode
            if (a is LambdaPatch) return a.successCode
            if (a is LambdaPut) return a.successCode
        }
        return SC_OK
    }

    /**
     * For a given path, parses out a list of parameters denoted by braces.
     *
     *
     * e.g., getPathParams('/v2/my-request/{someUuid}/{otherValue}')
     * Result:  { 'someUuid', 'otherValue' }
     *
     * @param path
     * @return List of path params as strings (without braces)
     */
    private fun getPathParams(path: String?): List<String> {
        val matches = mutableListOf<String>()
        val m: Matcher = Pattern.compile("\\{[a-zA-Z0-9]*\\}").matcher(path)
        while (m.find()) matches.add(m.group().replace("{", "").replace("}", ""))
        return matches
    }

    /**
     * Predicate: Given a method annotated with a matching HttpMethod annotation (e.g., @LambdaGet),
     * determines whether this handler matches an imcoming path.
     *
     * @param path
     * @param resource
     * @param endpoint
     * @param pathParams
     * @return boolean
     */
    private fun checkMatch(
        path: String,
        resource: String?,
        endpoint: String,
        pathParams: Map<String, String>
    ): Boolean {
        var pth = path
        if (endpoint == resource) return true
        val keys = pathParams.keys
        val endpointParams = getPathParams(endpoint)
        if (!keys.containsAll(endpointParams)) return false
        for (key in keys) {
            val value = "{$key}"
            pth = pth.replace(pathParams[key] ?: "", value)
        }

        // Default and vanity URLs return different paths to the APIGatewayV2ProxyRequestEvent object...
        // vanity prepends path, default did not.
        // Must use `.endsWith()`...  Can't even use `.contains()`, because the failure to differentiate `/v2/closest-location` and `/v2/closest-location-by-distance`
        return endpoint.endsWith(pth)
    }

    /**
     * Gets the Method for the given api lambda request.
     *
     *
     * Uses the path, and, if necessary, the list of path parameters from the APIGatewayV2ProxyRequest object.
     *
     *
     * If none found, returns null.
     *
     * @param method
     * @param path
     * @param resource
     * @param pathParams
     * @return Method
     */
    private fun lambdaHandler(
        method: String,
        path: String,
        resource: String?,
        pathParams: Map<String, String>
    ): Method? {
        for (m in this.javaClass.methods) {
            when (HttpMethod.valueOf(method.toUpperCase())) {
                HttpMethod.HEAD -> {
                    return null
                }
                HttpMethod.GET -> {
                    val get: LambdaGet? = m.getAnnotation(LambdaGet::class.java)
                    if (get == null || !checkMatch(path, resource, get.value, pathParams)) continue
                    return m
                }
                HttpMethod.POST -> {
                    val pst: LambdaPost? = m.getAnnotation(LambdaPost::class.java)
                    if (pst == null || !checkMatch(path, resource, pst.value, pathParams)) continue
                    return m
                }
                HttpMethod.DELETE -> {
                    val del: LambdaDelete? = m.getAnnotation(LambdaDelete::class.java)
                    if (del == null || !checkMatch(path, resource, del.value, pathParams)) continue
                    return m
                }
                HttpMethod.PATCH -> {
                    val pch: LambdaPatch? = m.getAnnotation(LambdaPatch::class.java)
                    if (pch == null || !checkMatch(path, resource, pch.value, pathParams)) continue
                    return m
                }
                HttpMethod.PUT -> {
                    val put: LambdaPut? = m.getAnnotation(LambdaPut::class.java)
                    if (put == null || !checkMatch(path, resource, put.value, pathParams)) continue
                    return m
                }
            }
        }
        return null
    }

    fun getEnumValue(enumClassName: String, enumValue: String): Any {
        val enumClz = Class.forName(enumClassName).enumConstants as Array<Enum<*>>
        return enumClz.first { it.name == enumValue }
    }

    /**
     * Given a String input, constructs an argument of a given type for a method parameter.
     *
     * @param str
     * @param type
     * @param p
     * @return Typed Parameter as Object
     */
    @SuppressWarnings("unchecked")
    private fun typeParameter(str: String?, type: Class<*>, p: Parameter): Any? {
        if (str == null) return null
        if (type === String::class.java) return str
        if (type === Integer::class.java) return Integer.parseInt(str)
        if (type === Double::class.java) return str.toDoubleOrNull()
        if (type === Float::class.java) return str.toFloatOrNull()
        if (type === Boolean::class.java) return str.toBoolean()
        if (type === UUID::class.java) return UUID.fromString(str)
        @Suppress("UPPER_BOUND_VIOLATED", "UNCHECKED_CAST", "TYPE_MISMATCH")
        if (type.isEnum)
            return java.lang.Enum.valueOf<Any>(type, str)
        if (type === List::class.java) {
            if (p.parameterizedType.typeName.equals("java.util.List<java.lang.String>", true)) {
                logger.info("Turning value {} into list of strings for parameter with name {}", str, p.name)
                return str.split(",")
            }
            if (p.parameterizedType.typeName.equals("java.util.List<java.util.UUID>", true)) {
                logger.info("Turning value {} into list of uuids for parameter with name {}", str, p.name)
                val value: MutableList<UUID> = mutableListOf()
                for (l in str.split(",")) value += UUID.fromString(l.trim())
                return value
            }
            logger.warn(
                "Wanted to convert parameter to a list with a yet unhandled generic type of {}",
                p.parameterizedType.typeName
            )
        }
        return str // Hard to test a fall through
    }

    /**
     * Gets a header value from an APIGatewayV2ProxyRequest object.  This method intentionally ignores key casing.
     *
     * @param headers
     * @param key
     * @return String containing header value correspoding to type-insensitive key
     */
    private fun getHeader(headers: Map<String, String>?, key: String): String? {
        if (headers != null) for (k in headers.keys) if (k.equals(key, true)) return headers[k]
        return null
    }

    /**
     * Invokes arbitrary method m, constructing the argument list with the other parameters as possible method parameters.
     * Allows for re-sequencing of args, as well as handling parameter annotations to fill in values from other sources
     * such as header, querystring, etc.
     *
     *
     * null is inserted for unknown parameter types.
     *
     * @param m
     * @param input
     * @param context
     * @param apiResponse
     * @return Returns result of invocation, if any, as Object
     */
    private fun invokeWithParams(
        m: Method,
        input: APIGatewayV2ProxyRequestEvent?,
        context: Context?,
        apiResponse: APIGatewayV2ProxyResponseEvent?
    ): Any? {
        val n: String = m.name
        return try {
            val args: Array<Any?> = buildParameters(m, input, context, apiResponse)
            m.isAccessible = true
            return m.invoke(this, *args)
        } catch (e: IllegalAccessException) {  // Can this happen since we set accessible?
            throw LambdaControllerProcessingException("Illegal Access in hook [" + n + "]: " + e.message, e)
        } catch (e: InvocationTargetException) {
            if (RuntimeException::class.java.isAssignableFrom(e.targetException.javaClass))
                throw e.targetException
            throw LambdaControllerProcessingException(e.targetException.message, e.targetException)
        }
    }

    /**
     * Build a paramter array (Object[]) for method m, regardless of sequence, looking for header, path, and querystring
     * parameters by annotations.
     *
     * @param m
     * @param input
     * @param context
     * @param response
     * @return Array of Objects representing the list of parameters for .invoke(...)
     */
    private fun buildParameters(
        m: Method,
        input: APIGatewayV2ProxyRequestEvent?,
        context: Context?,
        response: APIGatewayV2ProxyResponseEvent?
    ): Array<Any?> {
        val args: Array<Any?> = arrayOfNulls(m.parameterCount)
        var i = -1
        for (p in m.parameters) {
            i++
            if (p.type === APIGatewayV2ProxyRequestEvent::class.java) args[i] = input
            else if (p.type === APIGatewayV2ProxyResponseEvent::class.java) args[i] = response
            else if (p.type === Context::class.java) args[i] = context
            else if (p.type === JSONObject::class.java) args[i] = JSONObject(input?.body ?: "{}")
            else if (p.getAnnotation(LambdaPathParam::class.java) != null) {
                var strValue: String? = null
                var name: String = p.name
                if (!p.getAnnotation(LambdaPathParam::class.java).value.isEmpty()) name = p.getAnnotation(
                    LambdaPathParam::class.java
                ).value
                if (input?.pathParameters != null) strValue = input.pathParameters[name] ?: ""
                val value = typeParameter(strValue, p.type, p)
                args[i] = value
            } else if (p.getAnnotation(LambdaQueryStringParam::class.java) != null) {
                var strValue: String? = null
                var name: String = p.getName()
                if (!p.getAnnotation(LambdaQueryStringParam::class.java).value.isEmpty())
                    name = p.getAnnotation(LambdaQueryStringParam::class.java).value
                if (input?.queryStringParameters != null) strValue = input.queryStringParameters[name] ?: ""
                val value = typeParameter(strValue, p.type, p)
                args[i] = value
            } else if (p.getAnnotation(LambdaHeaderParam::class.java) != null) {
                var strValue: String? = null
                var name: String = p.name
                if (p.getAnnotation(LambdaHeaderParam::class.java).value.isNotEmpty())
                    name = p.getAnnotation(LambdaHeaderParam::class.java).value
                if (input?.headers != null) strValue = getHeader(input.headers, name)
                val value = typeParameter(strValue, p.type, p)
                args[i] = value
            } else if (p.getAnnotation(LambdaHeaderParameters::class.java) != null) {
                if (p.type === Map::class.java) args[i] = input?.headers
            } else if (p.getAnnotation(LambdaQueryStringParameters::class.java) != null) {
                if (p.type === Map::class.java) args[i] = input?.queryStringParameters
            } else if (p.getAnnotation(LambdaPathParameters::class.java) != null) {
                if (p.type === Map::class.java) args[i] = input?.pathParameters
            } else {
                val type: Class<*> = p.type
                val value = LambdaSupportUtil.readFromString<Any>(
                    input?.body,
                    type as Class<Any>,
                    "Unable to deserialize request body to type '${type.javaClass.name}': $$"
                )
                args[i] = value
            }
        }
        return args
    }

    /**
     * Execute all methods annotated with @LambdaWarmUp
     */
    private fun handleWarmUp() {
        executeHook(LambdaWarmUp::class.java, null, null, null)
    }

    /**
     * Execute all methods annotated with @LambdaPreProcess
     */
    private fun preProcessEvent(request: APIGatewayV2ProxyRequestEvent, context: Context) {
        executeHook(LambdaPreProcess::class.java, request, context, null)
    }

    /**
     * Execute all methods annotated with @LambdaPostProcess
     */
    private fun postProcessEvent(
        request: APIGatewayV2ProxyRequestEvent,
        context: Context,
        response: APIGatewayV2ProxyResponseEvent?
    ): APIGatewayV2ProxyResponseEvent? {
        executeHook(LambdaPostProcess::class.java, request, context, response)
        return response
    }

    /**
     * Execute all methods annotated with annotation anno.
     *
     * @param anno
     * @param request
     * @param context
     * @param response
     */
    private fun executeHook(
        anno: Class<out Annotation?>?,
        request: APIGatewayV2ProxyRequestEvent?,
        context: Context?,
        response: APIGatewayV2ProxyResponseEvent?
    ) {
        for (m in this.javaClass.methods) {
            val chk = m.getAnnotation(anno) ?: continue
            invokeWithParams(m, request, context, response)
        }
    }

    /**
     * Handles an error thrown either by an annotation hook or handler invocation.  This function
     * scans the class methods for methods annotated with @LambdaExceptionHandler with a throwable
     * type matching the value.
     *
     * @param t
     * @param method
     * @param input
     * @param context
     * @return APIGatewayV2ProxyResponseEvent
     */
    private fun handleError(
        t: Throwable,
        method: Method?,
        input: APIGatewayV2ProxyRequestEvent?,
        context: Context?
    ): APIGatewayV2ProxyResponseEvent? {
        logger.info("LambdaController::handleError - {}: {}", t.javaClass.simpleName, t.message)
        for (m in this.javaClass.methods) {
            val leh: LambdaExceptionHandler = m.getAnnotation(LambdaExceptionHandler::class.java) ?: continue
            val chk: KClass<out java.lang.Exception> = leh.value
            var status: Int = SC_BAD_REQUEST
            if (!chk.java.isAssignableFrom(t.javaClass)) continue
            if (LambdaControllerException::class.java.isAssignableFrom(t.javaClass))
                status = (t as LambdaControllerException).status
            return try {
                val errorResponse = m.invoke(this, t)
                if (errorResponse is APIGatewayV2ProxyResponseEvent) return errorResponse
                if (errorResponse is String) return LambdaSupportUtil.errorResponseEvent(errorResponse, status)
                if (errorResponse is Int) LambdaSupportUtil.errorResponseEvent("Error", status)
                // If we can't figure out what they gave us, just serialize it and give it to the client.
                else LambdaSupportUtil.errorResponseEvent(LambdaSupportUtil.writeToString(errorResponse), status)
            } catch (tErr: Exception) {
                logger.error("Failed dispatching error handler: {}: {}", m.getName(), tErr.message)
                LambdaSupportUtil.errorResponseEvent("Internal Error", SC_INTERNAL_SERVER_ERROR)
            }
        }
        return handleErrorDefault(t, method, input, context)
    }

    /**
     * Default error handler.  If no annotated error handler can process the message, this is the default
     * handling, specifically processing various explicit exception types.
     *
     * @param t
     * @param method
     * @param input
     * @param context
     * @return
     */
    private fun handleErrorDefault(
        t: Throwable,
        method: Method?,
        input: APIGatewayV2ProxyRequestEvent?,
        context: Context?
    ): APIGatewayV2ProxyResponseEvent? {
        logger.info("LambdaController::handleErrorDefault - {}: {}", t.javaClass.simpleName, t.message)
        val name = if (method == null) "--" else method.name
        var msg: String = t.message ?: t.toString()
        if (t is LambdaControllerException) {
            val status: Int = t.status
            return LambdaSupportUtil.errorResponseEvent(msg, status)
        }
        if (t is IllegalArgumentException) {
            msg = "Illegal Arguments: " + t.message
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_BAD_REQUEST)
        }
        if (t is BadRequestException) {
            msg = "Bad Request: " + t.message
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_BAD_REQUEST)
        }
        if (t is NotFoundException ||
            t.javaClass.name.equals("com.amazonaws.services.kms.model.NotFoundException", true)
        ) {
            msg = "Not Found: " + t.message
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_NOT_FOUND)
        }
        if (t is StringToObjectException) {
            msg = "Unable to deserialize object: " + t.message
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_BAD_REQUEST)
        }
        if (t is IllegalAccessException) {
            msg = "Unable to access target '$name': $msg"
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_INTERNAL_SERVER_ERROR)
        }
        if (t is InvocationTargetException) {
            msg = "Unable to invoke target '$name': $msg"
            logger.error(msg, t)
            return LambdaSupportUtil.errorResponseEvent(msg, SC_INTERNAL_SERVER_ERROR)
        }
        msg = "Error handling request: [" + t.javaClass.getSimpleName().toString() + "] " + msg
        logger.error(msg, t)
        return LambdaSupportUtil.errorResponseEvent(msg, SC_INTERNAL_SERVER_ERROR)
    }

    /**
     * Returns a unique list of classes in either the parameters or return types of handler methods.
     *
     * @return List
     */
    private val controllerClassModels: List<Any>
        get() {
            val objectModels: MutableList<Class<*>> = mutableListOf()
            for (m in this.javaClass.methods) {
                if (m.getAnnotationsByType(LambdaGet::class.java).isNotEmpty() ||
                    m.getAnnotationsByType(LambdaPost::class.java).isNotEmpty() ||
                    m.getAnnotationsByType(LambdaDelete::class.java).isNotEmpty() ||
                    m.getAnnotationsByType(LambdaPatch::class.java).isNotEmpty() ||
                    m.getAnnotationsByType(LambdaPut::class.java).isNotEmpty()
                ) {
                    val r: Class<*> = m.returnType
                    if (!r.isPrimitive && !r.isEnum && r !== String::class.java) objectModels += r
                    for (p in m.parameters) {
                        val t: Class<*> = p.type
                        if (!t.isPrimitive && !t.isEnum && t !== String::class.java) objectModels += r
                    }
                }
            }
            return objectModels
                .distinct()
                .filter { c: Class<*> -> getControllerClassModelsInclude(c) }
        }

    /**
     * Predicate determinig whether to include the given return type as a class model.
     *
     * @param c
     * @return boolean
     */
    private fun getControllerClassModelsInclude(c: Class<*>): Boolean {
        val ignoredClasses: List<String> = Arrays.asList(
            "void",
            "java.lang.Exception",
            "java.lang.String",
            "java.util.UUID",
            "java.util.List",
            "java.util.Map",
            "com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent",
            "com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent",
            "com.ford.avllc.geoservicelibrary.exception.ErrorResponse",
            "com.ford.avllc.geoservicelibrary.exception.LambdaControllerException",
            "com.ford.avllc.geoservicelibrary.exception.LambdaControllerProcessingException"
        )
        val n: String = c.getName()
        for (s in ignoredClasses) if (s.equals(n)) return false
        return true
    }

    /**
     * Designates a handler to warm-up the lambda (run on controller constructor which is provisioned before
     * the lambda is actually called.  Useful for initializing databases, object mappers, etc.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaWarmUp

    /**
     * Used to annotate a GET http method handler
     *
     *
     * String value: path (e.g., /v2/my-path/{pathParameter}
     * int successCode (default 200 SC_OK): Http status code for success
     * String method (default GET): used internally
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaGet(val value: String, val method: String = "GET", val successCode: Int = SC_OK)

    /**
     * Used to annotate a POST http method handler
     *
     *
     * String value: path (e.g., /v2/my-path/{pathParameter}
     * int successCode (default 200 SC_OK): Http status code for success
     * String method (default POST): used internally
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPost(val value: String, val method: String = "POST", val successCode: Int = SC_OK)

    /**
     * Used to annotate a DELETE http method handler
     *
     *
     * String value: path (e.g., /v2/my-path/{pathParameter}
     * int successCode (default 200 SC_OK): Http status code for success
     * String method (default DELETE): used internally
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaDelete(val value: String, val method: String = "DELETE", val successCode: Int = SC_OK)

    /**
     * Used to annotate a PATCH http method handler
     *
     *
     * String value: path (e.g., /v2/my-path/{pathParameter}
     * int successCode (default 200 SC_OK): Http status code for success
     * String method (default PATCH): used internally
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPatch(val value: String, val method: String = "PATCH", val successCode: Int = SC_OK)

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPut(val value: String, val method: String = "PUT", val successCode: Int = SC_OK)

    /**
     * Used to indicate the argument is a header field parameter.
     *
     *
     * String value: parameter name
     * optional boolean required: whether this parameter is required
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaHeaderParam(val value: String = "", val required: Boolean = true)

    /**
     * Used to indicate the argument is a path parameter.
     *
     *
     * String value: parameter name
     * optional boolean required: whether this parameter is required
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPathParam(val value: String, val required: Boolean = true)

    /**
     * Used to indicate the argument is a querystring parameter.
     *
     *
     * String value: parameter name
     * optional boolean required: whether this parameter is required
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaQueryStringParam(val value: String, val required: Boolean = true)

    /**
     * Used to indicate the argument is the entire path parameter mapping.
     *
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    @Deprecated("Use @LambdaPathParam to get individual path params where possible")
    annotation class LambdaPathParameters

    /**
     * Used to indicate the argument is the entire querystring mapping.
     *
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    @Deprecated("Use @LambdaQueryStringParam to get individual values where possible")
    annotation class LambdaQueryStringParameters

    /**
     * Used to indicate the argument is the entire header mapping.
     *
     */
    @Target(AnnotationTarget.VALUE_PARAMETER)
    @Retention(AnnotationRetention.RUNTIME)
    @Deprecated("Use @LambdaHeaderParam to get individual headers where possible")
    annotation class LambdaHeaderParameters

    /**
     * Used to designate an error handling function.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaExceptionHandler(val value: KClass<out java.lang.Exception>, val status: Int = 400)

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaDesc(val value: String, val tags: Array<String>)

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaApiGateway(val value: String)

    /**
     * Designates a method that can Pre-process the APIGatewayV2ProxyRequest before the handler has run.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPreProcess

    /**
     * Designates a method that can Post-process the APIGatewayV2ProxyResponse after the handler has run.
     *
     *
     * Not invoked if there is an error thrown in the handler.
     */
    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LambdaPostProcess

    init {
        if (warmupEnabled) {
            PropertyHandler.warmUp()
            LambdaSupportUtil.warmUpObjectMapperForClassList(controllerClassModels as List<Class<*>>)
            try {
                handleWarmUp()
            } catch (throwable: Throwable) {
                throw RuntimeException("LambdaController: Error in warm up: ", throwable)
            }
        }
    }
}
