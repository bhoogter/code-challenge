package com.example.integration

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.Context
import com.example.data.TestData.Companion.TEST_REQUEST_1
import com.example.data.TestData.Companion.TEST_REQUEST_2
import com.example.data.TestData.Companion.TEST_REQUEST_3
import com.example.controller.TaskController
import com.example.model.TaskResponse
import com.example.support.APIGatewayV2ProxyRequestEventBuilder
import com.example.support.LambdaSupportUtil
import junit.framework.Assert.assertEquals
import org.apache.http.HttpStatus.SC_OK
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TaskControllerIT {
    val taskController: TaskController = TaskController()
    val mockContext: Context = mock(Context::class.java)

    @Test
    fun testIT() {

        var apiRequest = APIGatewayV2ProxyRequestEventBuilder.builder()
            .path(TaskController.TASK_PATH)
            .httpMethod(HttpMethod.GET)
            .body(TEST_REQUEST_1)
            .build()

        var apiResponse = taskController.handleRequest(apiRequest, mockContext)
        assertEquals(SC_OK, apiResponse?.statusCode)
        var response = LambdaSupportUtil.readFromString(apiResponse?.body, TaskResponse::class.java)

        assertEquals(4, response?.count ?: -1)

        apiRequest.body = LambdaSupportUtil.writeToString(TEST_REQUEST_2)
        apiResponse = taskController.handleRequest(apiRequest, mockContext)
        response = LambdaSupportUtil.readFromString(apiResponse?.body, TaskResponse::class.java)
        assertEquals(6, response?.count ?: -1)

        apiResponse = taskController.handleRequest(apiRequest, mockContext)
        response = LambdaSupportUtil.readFromString(apiResponse?.body, TaskResponse::class.java)
        assertEquals(6, response?.count ?: -1)

        apiRequest.body = LambdaSupportUtil.writeToString(TEST_REQUEST_3)
        apiResponse = taskController.handleRequest(apiRequest, mockContext)
        response = LambdaSupportUtil.readFromString(apiResponse?.body, TaskResponse::class.java)
        assertEquals(9, response?.count ?: -1)
    }


}