package com.example.support.lambdacontroller

class TestLambdaException : RuntimeException {
    companion object {
        const val MESSAGE = "test lambda exception"
    }

    constructor() : super(MESSAGE)
}