package com.example.repository

open class TaskRepository {
    private var processed = mutableSetOf<Int>()
    var count = 0
        private set

    fun isProcessed(item: Int): Boolean = processed.contains(item)
    private fun setProcessed(item: Int) = processed.add(item)
    fun process(item: Int, msg: String): Int {
        setProcessed(item)
        count += msg.split(" ").size
        return count
    }

    fun reset() {
        processed = mutableSetOf()
        count = 0
    }
}
