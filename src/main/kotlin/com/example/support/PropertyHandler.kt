package com.example.support

import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml

class PropertyHandler {
    companion object {
        private val logger = LoggerFactory.getLogger(javaClass)
        private var properties: MutableMap<String, Any>? = null
        private var envProperties: MutableMap<String, Any>? = null
        private var testProperties: MutableMap<String, Any>? = null
        private var setProperties: MutableMap<String, Any>? = null
        var environment: String? = null
            set(env) {
                field = env
                if (environment != null && !environment!!.isEmpty()) envProperties =
                    loadYaml("application-" + environment + ".yml")
            }

        @JvmStatic
        fun warmUp() {
            logger.trace("Warming up property handler")
        }

        fun getSystemEnv(property: String): String? {
            return System.getenv(property)
        }

        fun getProperty(property: String): Any? {
            return getProperty(property, null)
        }

        fun getProperty(property: String, defaultValue: Any?): Any? {
            var result: Any? = null
            if (result == null) result = getYamlProperty(property, setProperties)
            if (result == null) result = getSystemEnv(envSlug(property))
            if (result == null && testProperties != null) result = getYamlProperty(property, testProperties)
            if (result == null && envProperties != null) result = getYamlProperty(property, envProperties)
            if (result == null && properties != null) result = getYamlProperty(property, properties)
            if (result == null) result = defaultValue
            logger.debug("PropertyHandler:  [{}] ==> {}", property, result)
            return result
        }

        fun setProperty(property: String, value: Any): Any {
            setProperties?.set(property, value)
            return value
        }

        fun setPropertyString(property: String, value: String): String {
            setProperty(property, value)
            return value
        }

        fun setPropertyInt(property: String, value: Int): Int {
            setProperty(property, value)
            return value
        }

        fun getPropertyString(property: String): String? {
            return getPropertyString(property, null)
        }

        @JvmStatic
        fun getPropertyString(property: String, defaultValue: String?): String? {
            val value: Any? = getProperty(property)
            return if (value == null) defaultValue else value.toString()
        }

        fun getPropertyInt(property: String): Int? {
            return getPropertyInt(property, null)
        }

        fun getPropertyInt(property: String, defaultValue: Int?): Int? {
            val result: Any? = getProperty(property, defaultValue)
            return if (result == null) defaultValue else Integer.parseInt(result.toString())
        }

        fun getPropertyBool(property: String): Boolean {
            return getPropertyBool(property, false)
        }

        fun getPropertyBool(property: String, defaultValue: Boolean): Boolean {
            val value: Any? = getProperty(property)
            return value?.toString()?.toBoolean() ?: defaultValue
        }

        internal val isJUnitTest: Boolean
            internal get() {
                var isJunitTest = false
                val stackTrace: Array<StackTraceElement> = Thread.currentThread().getStackTrace()
                val list: Array<StackTraceElement> = stackTrace
                for (element in list) if (element.getClassName().startsWith("org.junit.")) {
                    isJunitTest = true
                }
                if (isJunitTest) logger.debug("JUNIT TEST DETECTED")
                return isJunitTest
            }

        internal fun loadYaml(resourceFile: String?): MutableMap<String, Any>? {
            val yaml = Yaml()
            try {
                PropertyHandler::class.java.classLoader.getResourceAsStream(resourceFile)
                    .use { stream -> return yaml.load(stream) }
            } catch (e: Exception) {
                logger.warn(String.format("Unable to load yaml file for PropertyHandler: %s", resourceFile))
                return null
            }
        }

        @SuppressWarnings("unchecked")
        private fun getYamlProperty(property: String, loc: MutableMap<String, Any>?): Any? {
            var mloc: MutableMap<String, Any>? = loc!!
            if (mloc!!.containsKey(property)) return mloc[property]

            val keys = property.split(".")
            for (i in keys.indices) {
                val value: Any = mloc!![keys[i]] ?: return null
                if (i == keys.size - 1 && value !is MutableMap<*, *>) {
                    return value
                }
                if (value !is MutableMap<*, *>) {
                    // This shouldn't ever happen.  Sub objects are always hash maps
                    return null
                }
                mloc = value as MutableMap<String, Any>?
            }
            // If the key didn't exist, we would have returned null earlier.
            return null
        }

        private fun envSlug(property: String): String {
            var s = property
            s = s.toUpperCase()
            s = s.replace("-", "_")
            s = s.replace(".", "_")
            while (s.contains("__")) s = s.replace("__", "_")
            return s
        }

        init {
            setProperties = HashMap() // for all overridden properties
            testProperties = if (!isJUnitTest) null else loadYaml("application-test.yml")
            properties = loadYaml("application.yml")
            environment = getSystemEnv("ENVIRONMENT")
        }
    }
}
