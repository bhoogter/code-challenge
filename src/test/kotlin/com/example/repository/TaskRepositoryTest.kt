package com.example.repository

import junit.framework.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TaskRepositoryTest {

    @InjectMocks
    lateinit var taskRepository: TaskRepository

    @Test
    fun testItAll() {
        taskRepository.reset()
        assertEquals(0, taskRepository.count)

        taskRepository.process(1, "THE RAIN IN SPAIN")
        assertTrue(taskRepository.isProcessed(1))
        assertFalse(taskRepository.isProcessed(2))
        assertFalse(taskRepository.isProcessed(0))
        assertEquals(4, taskRepository.count)

        taskRepository.process(2, "FALLS MOSTLY")
        assertTrue(taskRepository.isProcessed(1))
        assertEquals(6, taskRepository.count)

        taskRepository.process(99, "THE PLAINS")
        assertTrue(taskRepository.isProcessed(99))
        assertFalse(taskRepository.isProcessed(3))
        assertEquals(8, taskRepository.count)

        taskRepository.reset()
        assertEquals(0, taskRepository.count)
        assertFalse(taskRepository.isProcessed(1))
        assertFalse(taskRepository.isProcessed(2))
        assertFalse(taskRepository.isProcessed(3))
        assertFalse(taskRepository.isProcessed(99))

        taskRepository.reset()
    }

}