package com.example.support

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.support.LambdaSupportUtil.Companion.writeToString
import com.example.support.exception.StringToObjectException
import com.example.support.model.DateHolder
import com.example.support.model.TestRequest
import com.example.support.model.TestResponse
import junit.framework.Assert.assertEquals
import org.apache.http.HttpStatus
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.time.ZonedDateTime
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class LambdaSupportUtilTest {
    @Test
    fun testSetCoorsHeaders() {
        val testResponse = APIGatewayV2ProxyResponseEvent()
        LambdaSupportUtil.setCorsHeaders(testResponse)
        Assert.assertEquals(5, testResponse.headers.size.toLong())
    }

    @Test
    fun testCreateErrorResponse() {
        val result = LambdaSupportUtil.errorResponseEvent("msg")
        Assert.assertEquals(HttpStatus.SC_BAD_REQUEST.toLong(), result.statusCode.toLong())
        Assert.assertTrue(result.body.contains("timeStamp"))
        Assert.assertTrue(result.body.contains("errorMessage"))
        Assert.assertTrue(result.body.contains("msg"))
    }

    @Test
    fun testResponseEvent() {
        val test = TestRequest()
        test.b = true
        val result = LambdaSupportUtil.responseEvent(HttpStatus.SC_MULTI_STATUS, test)
        Assert.assertEquals(HttpStatus.SC_MULTI_STATUS.toLong(), result.statusCode.toLong())
        Assert.assertTrue(result.body.contains("true"))
    }

    @Test
    fun testResponseEventAsString() {
        val test: TestRequest = TestRequest(b = true)
        val result = LambdaSupportUtil.responseEvent(HttpStatus.SC_MULTI_STATUS, writeToString(test))
        Assert.assertEquals(HttpStatus.SC_MULTI_STATUS.toLong(), result.statusCode.toLong())
        Assert.assertTrue(result.body.contains("true"))
    }

    @Test
    fun testWriteToString() {
        val test: TestRequest = TestRequest(b = true)
        val result = writeToString(test)
        Assert.assertTrue(result!!.contains("true"))
    }

    @Test
    fun testReadFromString() {
        var json = "{ \"valid\":\"" + true + "\"}"
        val result = LambdaSupportUtil.readFromString(json, TestResponse::class.java)!!
        Assert.assertTrue(result.valid!!)
        Assert.assertNull(result.jurisdiction)
        Assert.assertNull(result.valuesSub)
        Assert.assertNull(result.viability)
    }

    @Test(expected = StringToObjectException::class)
    fun testReadFromString_badJson_throwsException() {
        LambdaSupportUtil.readFromString("+D(JUNK---", TestResponse::class.java, "Throw Exception")
    }

    @Test
    fun test_readFromString_ignoresUnkownFields() {
        val json = "{ \"jurisdiction\":\"broad\", \"unknown-field\": \"dont error\" }"
        val request = LambdaSupportUtil.readFromString(json, TestResponse::class.java)
        assertEquals("broad", request?.jurisdiction ?: "")
    }

    @Test
    fun test_writeToString_dontOutputNullFields() {
        val request: TestRequest = TestRequest(b = true)
        val json = writeToString(request)
        val expected = "{\"b\":true}"
        Assert.assertEquals(expected, json)
    }

    @Test
    fun testGetQueryParams() {
        val request = APIGatewayV2ProxyRequestEvent()
        assertEquals(0, LambdaSupportUtil.getQueryParams(request).size)
        request.queryStringParameters = object : HashMap<String?, String?>() {
            init {
                put("a", "b")
                put("c", "d")
            }
        }
        assertEquals(2, LambdaSupportUtil.getQueryParams(request).size)
    }

    @Test
    fun testGetPathParams() {
        val request = APIGatewayV2ProxyRequestEvent()
        assertEquals(0, LambdaSupportUtil.getPathParams(request).size)
        request.pathParameters = object : HashMap<String?, String?>() {
            init {
                put("a", "b")
                put("c", "d")
            }
        }
        assertEquals(2, LambdaSupportUtil.getPathParams(request).size)
    }

    @Test
    fun testDateSerializatonAndDe() {
        val value = "2020-06-03T15:48:28.686-04:00"
        val test = DateHolder()
        test.date = ZonedDateTime.parse(value)
        val result = writeToString(test)
        Assert.assertTrue(result!!.contains(value))
        val deserialize = LambdaSupportUtil.readFromString(result, DateHolder::class.java)
        Assert.assertTrue(test.date!!.isEqual(deserialize?.date ?: ZonedDateTime.now().minusSeconds(38)))
    }

    @Test
    fun testWarmUpByObject() {
        LambdaSupportUtil.warmUpObjectMapperForObject(Any())
        LambdaSupportUtil.warmUpObjectMapperForObjectList(Arrays.asList(Any()))
    }

    @Test
    fun testWarmUpByClass() {
        LambdaSupportUtil.warmUpObjectMapperForClass(Any::class.java)
        LambdaSupportUtil.warmUpObjectMapperForClassList(Arrays.asList(Any::class.java))
    }
}
