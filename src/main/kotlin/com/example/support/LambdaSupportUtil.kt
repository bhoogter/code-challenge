package com.example.support

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.constant.ErrorMessages.Companion.ERROR_CONVERTING_OBJECT_TO_BYTES
import com.example.constant.ErrorMessages.Companion.ERROR_CONVERTING_OBJECT_TO_STRING
import com.example.constant.HeaderConstants.Companion.HEADERS_CORS_HEADERS
import com.example.constant.HeaderConstants.Companion.HEADER_CONTENT_TYPE
import com.example.constant.HeaderConstants.Companion.HEADER_CORS_METHODS
import com.example.constant.HeaderConstants.Companion.HEADER_CORS_ORIGIN
import com.example.constant.HeaderConstants.Companion.HEADER_X_CUSTOM_HEADER
import com.example.support.exception.ObjectToBytesException
import com.example.support.exception.ObjectToStringException
import com.example.support.exception.StringToObjectException
import com.example.support.model.ErrorResponse
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.HttpStatus
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime

class LambdaSupportUtil {
    companion object {
        private var OBJECT_MAPPER: ObjectMapper = ObjectMapper()
        private val logger = LoggerFactory.getLogger(javaClass)
        private var warmUpComplete = false
        fun warmUpObjectMapper() {
            if (warmUpComplete) return
            warmUpComplete = true
            // Perform a few dummy operations to ensure the mapper wont load as much on an actual request handling
            readFromString("{}", ErrorResponse::class.java)
            writeToString(ErrorResponse())
        }

        fun warmUpObjectMapperForObject(instance: Any) {
            logger.info("Warming up object mapper for: " + instance.javaClass.simpleName)
            try {
                readFromString("{}", instance.javaClass)
                writeToString(instance)
            } catch (e: Exception) {
                // Ignore warmup errors.
            }
        }

        fun warmUpObjectMapperForClass(instance: Class<*>) {
            try {
                val o = instance.getDeclaredConstructor().newInstance()
                warmUpObjectMapperForObject(o)
            } catch (e: Exception) {
                // Ignore warmup errors.
            }
        }

        fun warmUpObjectMapperForObjectList(instances: List<Any>) {
            warmUpObjectMapper()
            for (o in instances) warmUpObjectMapperForObject(o)
        }

        fun warmUpObjectMapperForClassList(controllerClassModels: List<Class<*>>) {
            warmUpObjectMapper()
            for (c in controllerClassModels) warmUpObjectMapperForClass(c)
        }

        fun setCorsHeaders(response: APIGatewayV2ProxyResponseEvent): APIGatewayV2ProxyResponseEvent {
            var headers = response.headers
            if (headers == null) headers = HashMap()
            if (!headers.containsKey(HEADER_CONTENT_TYPE)) headers[HEADER_CONTENT_TYPE] = "application/json"
            if (!headers.containsKey(HEADER_X_CUSTOM_HEADER)) headers[HEADER_X_CUSTOM_HEADER] = "application/json"
            if (!headers.containsKey(HEADER_CORS_ORIGIN)) headers[HEADER_CORS_ORIGIN] = "*"
            if (!headers.containsKey(HEADER_CORS_METHODS)) headers[HEADER_CORS_METHODS] = "*"
            if (!headers.containsKey(HEADERS_CORS_HEADERS)) headers[HEADERS_CORS_HEADERS] = "*"
            response.headers = headers
            return response
        }

        fun getQueryParams(input: APIGatewayV2ProxyRequestEvent?): Map<String, String> {
            return if (input == null || input.queryStringParameters == null) HashMap() else input.queryStringParameters
        }

        fun getPathParams(input: APIGatewayV2ProxyRequestEvent?): Map<String, String> {
            return if (input == null || input.pathParameters == null) HashMap() else input.pathParameters
        }

        @JvmOverloads
        fun errorResponseEvent(
            errorMessage: String?,
            status: Int = HttpStatus.SC_BAD_REQUEST
        ): APIGatewayV2ProxyResponseEvent {
            return setCorsHeaders(
                APIGatewayV2ProxyResponseEventBuilder.builder()
                    .statusCode(status)
                    .body(ErrorResponse(ZonedDateTime.now(), errorMessage ?: ""))
                    .build()
            )
        }

        fun responseEvent(status: Int, body: String?): APIGatewayV2ProxyResponseEvent {
            return setCorsHeaders(
                APIGatewayV2ProxyResponseEventBuilder.builder()
                    .statusCode(status)
                    .body(body)
                    .build()
            )
        }

        fun responseEvent(status: Int, body: Any?): APIGatewayV2ProxyResponseEvent {
            return setCorsHeaders(
                APIGatewayV2ProxyResponseEventBuilder.builder()
                    .statusCode(status)
                    .body(body)
                    .build()
            )
        }

        @JvmStatic
        fun writeToString(obj: Any?): String? {
            return if (obj == null) null else try {
                OBJECT_MAPPER.writeValueAsString(obj)
            } catch (e: Exception) {
                val msg: String = String.format(ERROR_CONVERTING_OBJECT_TO_STRING, e.message, obj)
                throw ObjectToStringException(msg, e)
            }
        }

        fun writeToBytes(obj: Any?): ByteArray? {
            return if (obj == null) null else try {
                OBJECT_MAPPER.writeValueAsBytes(obj)
            } catch (e: Exception) {
                val msg: String =
                    String.format(ERROR_CONVERTING_OBJECT_TO_BYTES, e.message, obj)
                throw ObjectToBytesException(msg, e)
            }
        }

        fun <T> readFromString(input: String?, c: Class<T>, failureMessage: String = "$$"): T? {
            var failureMessage = failureMessage
            return try {
                OBJECT_MAPPER.readValue(input, c)
            } catch (ex: Exception) {
                if (ex.localizedMessage != null) failureMessage = failureMessage.replace("$$", ex.localizedMessage)
                if (failureMessage != "") throw StringToObjectException(failureMessage, ex)
                null
            }
        }

        init {
            OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL)
            OBJECT_MAPPER.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
            OBJECT_MAPPER.findAndRegisterModules()
        }
    }
}