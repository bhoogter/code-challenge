package com.example.support.lambdacontroller

import com.amazonaws.HttpMethod
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.example.support.APIGatewayV2ProxyRequestEventBuilder
import com.example.support.LambdaSupportUtil
import com.example.support.lambdacontroller.controller.TestControllerForBenchmarking
import com.example.support.lambdacontroller.controller.TestControllerForBenchmarking.Companion.CHECK
import com.example.support.model.TestRequest
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import java.util.function.ToDoubleFunction
import java.util.stream.Collectors

@RunWith(MockitoJUnitRunner::class)
class LambdaControllerBenchmarkTest : TestCase() {
    private lateinit var apiRequestGet: APIGatewayV2ProxyRequestEvent
    private lateinit var apiRequestPst: APIGatewayV2ProxyRequestEvent
    private lateinit var apiRequestDel: APIGatewayV2ProxyRequestEvent
    private lateinit var testController: TestControllerForBenchmarking

    @Before
    fun setup() {
        apiRequestGet = testRequest(HttpMethod.GET.toString())
        apiRequestPst = testRequest(HttpMethod.POST.toString())
        apiRequestDel = testRequest(HttpMethod.DELETE.toString())
        testController = TestControllerForBenchmarking()
    }

    @Test
    fun testNoopGet() {
        val before = System.currentTimeMillis()
        val apiResponse = testController.handleRequest(apiRequestGet, Mockito.mock(Context::class.java))
        val after = System.currentTimeMillis()
        assertEquals("Ok - GET", apiResponse?.body)
        System.err.println("Get Controller Cost: " + (after - before))
        //        assertTrue((after - before) < 100);
    }

    @Test
    fun testNoopPost() {
        val before = System.currentTimeMillis()
        val apiResponse = testController.handleRequest(
            apiRequestPst, Mockito.mock(
                Context::class.java
            )
        )
        val after = System.currentTimeMillis()
        assertEquals("Ok - POST", apiResponse?.body)
        System.err.println("Post Controller Cost: " + (after - before))
        //        assertTrue((after - before) < 100);
    }

    @Test
    fun testNoopDelete() {
        val before = System.currentTimeMillis()
        val apiResponse = testController.handleRequest(apiRequestDel, Mockito.mock(Context::class.java))
        val after = System.currentTimeMillis()
        assertEquals("Ok - DELETE", apiResponse?.body)
        System.err.println("Delete Controller Cost: " + (after - before))
        //        assertTrue((after - before) < 100);
    }

    @Test
    fun testNoopGetMulti() {
        val nums = tryN(50, HttpMethod.GET.toString())
        val average =
            nums.stream().mapToDouble(ToDoubleFunction { a: Long? -> a?.toDouble() ?: 0.0 }).average().asDouble
        System.err.println(
            "Multi Get Controller Cost: (" + average + "ms): " + nums.stream().map { obj: Long -> obj.toString() }
                .collect(Collectors.joining(", ")))
        assertTrue(true)
    }

    @Test
    fun testNoopPostMulti() {
        val nums = tryN(50, HttpMethod.POST.toString())
        val average =
            nums.stream().mapToDouble(ToDoubleFunction { a: Long? -> a?.toDouble() ?: 0.0 }).average().asDouble
        System.err.println(
            "Multi Get Controller Cost: (" + average + "ms): " + nums.stream().map { obj: Long -> obj.toString() }
                .collect(Collectors.joining(", ")))
        assertTrue(true)
    }

    @Test
    fun testNoopDeleteMulti() {
        val nums = tryN(50, HttpMethod.DELETE.toString())
        val average =
            nums.stream().mapToDouble(ToDoubleFunction { a: Long? -> a?.toDouble() ?: 0.0 }).average().asDouble
        System.err.println(
            "Multi Get Controller Cost: (" + average + "ms): " + nums.stream().map { obj: Long -> obj.toString() }
                .collect(Collectors.joining(", ")))
        assertTrue(true)
    }

    private fun tryN(n: Int, method: String): List<Long> {
        val result: MutableList<Long> = ArrayList()
        for (i in 0 until n) {
            val before = System.currentTimeMillis()
            val apiResponse = testController.handleRequest(byMethod(method)!!, Mockito.mock(Context::class.java))
            val after = System.currentTimeMillis()
            result.add(after - before)
            if (!(apiResponse?.body?.startsWith("Ok") ?: false)) fail("Not Ok")
        }
        return result
    }

    private fun byMethod(method: String): APIGatewayV2ProxyRequestEvent? {
        return when (method) {
            "GET" -> apiRequestGet
            "POST" -> apiRequestPst
            "DELETE" -> apiRequestDel
            else -> null
        }
    }

    private fun testRequest(method: String): APIGatewayV2ProxyRequestEvent {
        val testRequest = TestRequest(b=true)
        return APIGatewayV2ProxyRequestEventBuilder.builder()
            .httpMethod(method)
            .path(CHECK)
            .body(LambdaSupportUtil.writeToString(testRequest))
            .headers(object : HashMap<String?, String?>() {
                init {
                    put("x-user-id", TEST_UUID)
                    put("x-tenant-id", TEST_UUID + "," + UUID.randomUUID().toString())
                }
            })
            .build()
    }

    companion object {
        const val TEST_UUID = "94dd927b-942e-4ff5-8c7b-b50899f5efe2"
    }
}