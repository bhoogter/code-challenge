package com.example.support

import org.apache.http.HttpStatus
import org.junit.Assert
import org.junit.Test

class APIGatewayV2ProxyResponseEventBuilderTest {
    @Test
    fun testBuilder() {
        val response = APIGatewayV2ProxyResponseEventBuilder.builder()
            .statusCode(HttpStatus.SC_MULTI_STATUS)
            .body("{}")
            .build()
        Assert.assertEquals(HttpStatus.SC_MULTI_STATUS.toLong(), response.statusCode.toLong())
        Assert.assertEquals("{}", response.body)
    }
}