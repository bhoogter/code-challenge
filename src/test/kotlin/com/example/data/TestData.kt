package com.example.data

import com.example.model.TaskRequest

class TestData {
    companion object {
        const val TEST_MSG_1 = "The rain in spain"
        const val TEST_MSG_2 = "falls mostly"
        const val TEST_MSG_3 = "on the plains"

        val TEST_REQUEST_1 = TaskRequest(id = 1, message = TEST_MSG_1)
        val TEST_REQUEST_2 = TaskRequest(id = 2, message = TEST_MSG_2)
        val TEST_REQUEST_3 = TaskRequest(id = 3, message = TEST_MSG_3)
    }
}