package com.example.constant

class HeaderConstants {
    companion object {
        const val HEADER_CONTENT_TYPE = "content-type"
        const val HEADER_X_CUSTOM_HEADER = "x-custom-header"
        const val HEADER_CORS_ORIGIN = "access-control-allow-origin"
        const val HEADER_CORS_METHODS = "access-control-allow-methods"
        const val HEADERS_CORS_HEADERS = "access-control-allow-headers"

        // Custom header for gradle version on response.
        const val LAMBDA_VERSION_KEY = "x-lambda-version"
    }
}