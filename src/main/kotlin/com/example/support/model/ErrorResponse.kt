package com.example.support.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.ZonedDateTime


data class ErrorResponse(
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    val timeStamp: ZonedDateTime = ZonedDateTime.now(),
    val errorMessage: String = ""
)