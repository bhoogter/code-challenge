package com.example.support

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent

class APIGatewayV2ProxyResponseEventBuilder : APIGatewayV2ProxyResponseEvent() {
    fun build(): APIGatewayV2ProxyResponseEvent {
        return this
    }

    fun statusCode(s: Int): APIGatewayV2ProxyResponseEventBuilder {
        statusCode = s
        return this
    }

    fun body(s: String?): APIGatewayV2ProxyResponseEventBuilder {
        body = s
        return this
    }

    fun body(o: Any?): APIGatewayV2ProxyResponseEventBuilder {
        body = LambdaSupportUtil.writeToString(o)
        return this
    }

    companion object {
        fun builder(): APIGatewayV2ProxyResponseEventBuilder {
            return APIGatewayV2ProxyResponseEventBuilder()
        }
    }
}