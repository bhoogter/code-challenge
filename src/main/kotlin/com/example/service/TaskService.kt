package com.example.service

import com.example.model.TaskRequest
import com.example.model.TaskResponse
import com.example.repository.TaskRepository

open class TaskService {
    private var taskRepository: TaskRepository

    constructor() : this(TaskRepository())
    constructor(taskRepository: TaskRepository) {
        this.taskRepository = taskRepository
    }

    fun performTask(request: TaskRequest): TaskResponse {
        if (!taskRepository.isProcessed(request.id))
            taskRepository.process(request.id, request.message)
        return TaskResponse(taskRepository.count);
    }
}