package com.example.support.model

import java.io.Serializable

class TestResponseSub : Serializable {
    var x = 0
    var y = 0.0
    var jurisdiction: String? = null
    var viability: Double? = null

    constructor() {}
    constructor(x: Int, y: Int) {
        this.x = x
        this.y = y.toDouble()
    }
}