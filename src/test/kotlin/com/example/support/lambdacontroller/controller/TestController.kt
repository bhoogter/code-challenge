package com.example.support.lambdacontroller.controller

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.example.support.APIGatewayV2ProxyResponseEventBuilder
import com.example.support.LambdaController
import com.example.support.lambdacontroller.LambdaControllerTest
import com.example.support.lambdacontroller.TestLambdaException
import com.example.support.lambdacontroller.enums.TestControllerEnum
import com.example.support.lambdacontroller.exception.TestLambdaExtendedException
import com.example.support.lambdacontroller.exception.TestLambdaUnknownException
import com.example.support.model.ErrorResponse
import com.example.support.model.TestRequest
import com.example.support.model.TestResponse
import org.apache.http.HttpStatus
import org.junit.Assert
import org.junit.Assert.assertEquals
import java.time.ZonedDateTime
import java.util.*

class TestController : LambdaController() {
    @LambdaGet(NOOP)
    fun doNoop(): String {
        return "Ok"
    }

    @LambdaGet(V_1_GET_RESPONSE)
    @LambdaDesc(value = "Get some resposne", tags = ["a", "b", "c"])
    fun getResponse(
        testRequest: TestRequest?,
        @LambdaHeaderParam("x-uuid") uuid: UUID?,
        @LambdaHeaderParam("x-tenant-id") tenantIds: List<UUID?>,
        @LambdaHeaderParam("X-extra") extra: String?
    ): TestResponse {
        Assert.assertTrue(uuid is UUID)
        Assert.assertEquals(2, tenantIds.size.toLong())
        Assert.assertNull(extra)
        return LambdaControllerTest.testResponse()
    }

    @LambdaGet(V_1_GET_RESPONSE_STRINGS)
    fun getResponseStrings(
        testRequest: TestRequest?,
        @LambdaHeaderParam("x-uuid") uuid: UUID?,
        @LambdaHeaderParam("x-tenant-id") tenantIds: List<String?>,
        @LambdaHeaderParam("X-extra") extra: String?
    ): TestResponse {
        Assert.assertEquals(2, tenantIds.size.toLong())
        return LambdaControllerTest.testResponse()
    }

    @LambdaGet(V_1_GET_RESPONSE_VIN)
    @LambdaApiGateway("GetVinLambda")
    @LambdaDesc(value = "A Lambda To Get a Response Object", tags = ["a", "c", "d"])
    fun getResponseVin(
        testRequest: TestRequest?,
        @LambdaHeaderParam("x-uuid") uuid: UUID?,
        @LambdaHeaderParam("x-tenant-id") tenantIds: List<String?>,
        @LambdaHeaderParam("X-extra") extra: String?,
        @LambdaPathParam("vin") vin: String
    ): TestResponse {
        Assert.assertEquals(2, tenantIds.size.toLong())
        assertEquals(LambdaControllerTest.TEST_VIN, vin)
        val t: TestResponse =
            LambdaControllerTest.testResponse()
        t.jurisdiction = vin
        return t
    }

    @LambdaPut(V_1_PUT_ERROR)
    fun getErrorHandler(testRequest: TestRequest?): TestResponse {
        lastRequest = testRequest
        throw com.example.support.lambdacontroller.TestLambdaException()
    }

    @LambdaDelete(V_1_DELETE_ERROR_2)
    fun getErrorHandler2(
        testRequest: TestRequest?
    ): TestResponse {
        lastRequest = testRequest
        throw TestLambdaExtendedException(HttpStatus.SC_UNPROCESSABLE_ENTITY)
    }

    @LambdaPatch(V_1_PATCH_ERROR_3)
    fun getUnknownError(
        testRequest: TestRequest?
    ): TestResponse {
        lastRequest = testRequest
        throw TestLambdaUnknownException()
    }

    @LambdaPost(V_1_POST_VOID)
    fun getPstVoid(testRequest: TestRequest?) {
        lastRequest = testRequest
    }

    @LambdaGet(V_1_GET_QS)
    fun getGetForQS(
        @LambdaQueryStringParam("serviceAreaId") serviceAreaId: UUID?,
        @LambdaQueryStringParam("four") four: String?,
        @LambdaHeaderParameters headers: Map<String?, String?>,
        @LambdaPathParameters pathParams: Map<String?, String?>,
        @LambdaQueryStringParameters qs: Map<String?, String?>
    ): TestResponse {
        Assert.assertEquals("4", four)
        Assert.assertTrue(serviceAreaId is UUID)
        Assert.assertEquals(0, headers.size.toLong())
        Assert.assertEquals(0, pathParams.size.toLong())
        Assert.assertEquals(2, qs.size.toLong())
        return LambdaControllerTest.testResponse()
    }

    @LambdaPost(V_1_GET_QS)
    fun getPostForQSWithEnum(
        @LambdaQueryStringParam("enum") enumValue: TestControllerEnum?
    ): TestResponse {
        if (enumValue == null) Assert.fail("no enum passed")
        if (enumValue != TestControllerEnum.VALUE1) Assert.fail("wrong enum detected")
        return LambdaControllerTest.testResponse()
    }

    @LambdaExceptionHandler(TestLambdaException::class)
    fun handleTestException(t: TestLambdaException?): APIGatewayV2ProxyResponseEvent {
        handledError = true
        return APIGatewayV2ProxyResponseEventBuilder.builder()
            .body(
                ErrorResponse(ZonedDateTime.now(), LambdaControllerTest.ERROR_TEXT)
            )
            .build()
    }

    @LambdaExceptionHandler(TestLambdaExtendedException::class)
    fun handleTestException2(t: Throwable?): String {
        return TestLambdaExtendedException.Companion.MESSAGE
    }

    @LambdaPreProcess
    fun preProcessor() {
        System.err.println("before call")
    }

    @LambdaPostProcess
    fun postProcessor(response: APIGatewayV2ProxyResponseEvent?) {
        System.err.println("after call: " + (response?.toString() ?: "null"))
    }

    companion object {
        const val NOOP = "/noop"
        const val V_1_GET_RESPONSE = "/v1/get-response"
        const val V_1_GET_RESPONSE_STRINGS = "/v1/get-response-strings"
        const val V_1_GET_RESPONSE_VIN = "/v1/get-response/{vin}"
        val V_1_GET_RESPONSE_VIN1 =
            "/v1/get-response/" + LambdaControllerTest.TEST_VIN
        const val V_1_PUT_ERROR = "/v1/get-error"
        const val V_1_DELETE_ERROR_2 = "/v1/delete-error-2"
        const val V_1_PATCH_ERROR_3 = "/v1/patch-error-3"
        const val V_1_POST_VOID = "/v4/post-void"
        const val V_1_GET_QS = "/v88/get-for-qs"
        var lastRequest: TestRequest? = null
        var handledError = false
    }
}